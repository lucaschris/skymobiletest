package mobile.skynet.app.views.recyclerview.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Callback
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import mobile.skynet.app.R
import mobile.skynet.app.models.Movie
import mobile.skynet.app.utils.LoadImage
import mobile.skynet.app.views.recyclerview.actions.AdapterOnItemClickListener
import mobile.skynet.app.views.recyclerview.viewholder.ViewHolderMovieList
import java.lang.Exception

class AdapterMovieList(private val adapterOnItemClickListener: AdapterOnItemClickListener<Movie>, private val movies: List<Movie>)
    : RecyclerView.Adapter<ViewHolderMovieList>() {

    lateinit var picasso: Picasso

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderMovieList {
        val root = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_layout_movie, parent, false)
        this.picasso = Picasso.get()
        return ViewHolderMovieList(root)
    }

    override fun getItemCount(): Int = movies.size

    override fun onBindViewHolder(holder: ViewHolderMovieList, position: Int) {
        val movie = movies[position]

        LoadImage.load(picasso, movie.urlCoverImage
            , R.drawable.placeholder
            , R.drawable.image_not_found
            , holder.imageViewMovie
            , "CACHE: ${movie.urlCoverImage} ${movie.title}"
            , "CACHE: ${movie.urlCoverImage} ${movie.title}")

        holder.textViewTitle.text = movie.title
        holder.onBindOnClick(adapterOnItemClickListener, movie)
    }
}