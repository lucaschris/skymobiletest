package mobile.skynet.app.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import mobile.skynet.app.R
import mobile.skynet.app.views.fragments.BaseFragment
import mobile.skynet.app.views.fragments.FragmentListMovie

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val baseFragment : BaseFragment = FragmentListMovie.newInstance()

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.layout_support_fragment, baseFragment, baseFragment.tagFragment)
            .addToBackStack(baseFragment.tagFragment)
            .commit()
    }
}
