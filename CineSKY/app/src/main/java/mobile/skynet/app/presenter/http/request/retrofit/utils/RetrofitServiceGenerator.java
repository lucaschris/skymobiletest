package mobile.skynet.app.presenter.http.request.retrofit.utils;

import okhttp3.OkHttpClient;
import retrofit2.Converter;
import retrofit2.Retrofit;


import java.util.concurrent.TimeUnit;

/**
 * Created by r028367 on 16/11/2017.
 */

public class RetrofitServiceGenerator {

    public static <Clazz> Clazz getService(Class<Clazz> clazz, Converter.Factory factory, String baseUrl) {
        return getRetrofitInstance(factory, baseUrl).create(clazz);
    }


    private static Retrofit getRetrofitInstance(Converter.Factory factory, String url) {
        OkHttpClient.Builder builderHttpClient = new OkHttpClient.Builder();
        OkHttpClient httpClient = builderHttpClient.build();
        builderHttpClient.connectTimeout(1, TimeUnit.MINUTES);
        builderHttpClient.readTimeout(30, TimeUnit.SECONDS);
        builderHttpClient.writeTimeout(30, TimeUnit.SECONDS);
        Retrofit.Builder builderRetrofit = new Retrofit
                .Builder()
                .baseUrl(url)
                .addConverterFactory(factory);
        return builderRetrofit.client(httpClient).build();
    }


    private static Retrofit getRetrofitInstance(Converter.Factory factory, MillisecondTimeOutRequest timeOutRequest, String url) {
        OkHttpClient.Builder builderHttpClient = new OkHttpClient.Builder();
        OkHttpClient httpClient = builderHttpClient.build();
        builderHttpClient.connectTimeout(timeOutRequest.getConnectionTimeoutInMillis(), TimeUnit.MILLISECONDS);
        builderHttpClient.readTimeout(timeOutRequest.getReadTimeoutInMillis(), TimeUnit.MILLISECONDS);
        builderHttpClient.writeTimeout(timeOutRequest.getWriteTimeoutInMillis(), TimeUnit.MILLISECONDS);
        Retrofit.Builder builderRetrofit = new Retrofit
                .Builder()
                .baseUrl(url)
                .addConverterFactory(factory);
        return builderRetrofit.client(httpClient).build();
    }
}
