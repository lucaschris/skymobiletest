package mobile.skynet.app.views.fragments


import android.app.ProgressDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

import mobile.skynet.app.R
import mobile.skynet.app.models.Movie
import mobile.skynet.app.presenter.http.request.APIGetListMovie
import mobile.skynet.app.presenter.http.request.retrofit.action.CallbackRequestAPI
import mobile.skynet.app.presenter.http.request.retrofit.models.SimpleMessageCallback
import mobile.skynet.app.views.recyclerview.actions.AdapterOnItemClickListener
import mobile.skynet.app.views.recyclerview.adapter.AdapterMovieList


/**
 * A simple [Fragment] subclass.
 * Use the [FragmentListMovie.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class FragmentListMovie : BaseFragment(), AdapterOnItemClickListener<Movie> {


    override fun onClick(item: Movie) {
        val baseFragment : BaseFragment =  FragmentMovieDetails.newInstance(item)
        activity?.supportFragmentManager
            ?.beginTransaction()
            ?.replace(R.id.layout_support_fragment, baseFragment, baseFragment.tagFragment)
            ?.addToBackStack(baseFragment.tagFragment)
            ?.commit()
    }

    private lateinit var adapterMovieList: AdapterMovieList
    private var movies : ArrayList<Movie> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tagFragment = javaClass.simpleName
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?
                              , savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_list_movie, container, false)
        val listViewMovie = rootView.findViewById<RecyclerView>(R.id.recycler_view_list_movie)

        movies = if (savedInstanceState != null)
            savedInstanceState.getParcelableArrayList<Movie>(BUNDLE_MOVIES) as ArrayList<Movie>
        else
            arrayListOf()

        activity?.title = getString(R.string.title_fragment_list_movies)

        adapterMovieList = AdapterMovieList(this, movies = movies)
        listViewMovie.layoutManager = GridLayoutManager(context, 2)
        listViewMovie.adapter = adapterMovieList
        return rootView
    }

    override fun onResume() {
        super.onResume()

        val progressDialog = ProgressDialog(context)

        val delegateRequestMovies = object: CallbackRequestAPI<Movie, SimpleMessageCallback>  {

            override fun onSuccessRequest(data: Movie) {}

            override fun onSuccessRequest(data: List<Movie>) {
                dismissProgressDialog()
                movies.addAll(data)
                adapterMovieList.notifyDataSetChanged()
            }

            override fun onFailureRequest(message: SimpleMessageCallback) {
                dismissProgressDialog()
                Toast.makeText(context, message.message, Toast.LENGTH_LONG).show()
            }

            private fun dismissProgressDialog() {
                if (activity != null && activity?.isFinishing == false) {
                    activity?.runOnUiThread {
                        Runnable { progressDialog.dismiss() }
                    }
                }
            }
        }

        if (movies.isEmpty()) {
            progressDialog.setTitle("Pesquisar Filmes")
            progressDialog.setMessage("Aguarde alguns instantes enquanto pesquisamos os filmes disponíveis")
            progressDialog.setCancelable(false)
            progressDialog.isIndeterminate = true

            if (activity != null && activity?.isFinishing == false) {
                activity?.runOnUiThread {
                    Runnable { progressDialog.show() }
                }
            }
            APIGetListMovie().requestListMovie("https://sky-exercise.herokuapp.com", delegateRequestMovies)
        }

        else {
            adapterMovieList.notifyDataSetChanged()
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            movies = savedInstanceState.getParcelableArrayList<Movie>(BUNDLE_MOVIES) as ArrayList<Movie>
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(BUNDLE_MOVIES, movies)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentListMovie.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            FragmentListMovie().apply {}

        private const val BUNDLE_MOVIES : String = "BUNDLE_MOVIES"
    }
}
