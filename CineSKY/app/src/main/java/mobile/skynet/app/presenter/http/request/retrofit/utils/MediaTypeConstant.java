package mobile.skynet.app.presenter.http.request.retrofit.utils;

public class MediaTypeConstant {
    public static final String MEDIA_TYPE_JSON = "application/json; charset=utf-8";
}
