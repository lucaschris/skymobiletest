package mobile.skynet.app.presenter.http.request.retrofit.converter;

public interface IConverterResponseGeneric<T> {
    T convertResponseOnObject(String body);
}
