package mobile.skynet.app.views.recyclerview.viewholder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mobile.skynet.app.R
import mobile.skynet.app.models.Movie
import mobile.skynet.app.views.recyclerview.actions.AdapterOnItemClickListener

class ViewHolderMovieList(private val rootLayout: View) : RecyclerView.ViewHolder(rootLayout) {
    var imageViewMovie : ImageView = rootLayout.findViewById(R.id.main_image_movie)
    var textViewTitle: TextView = rootLayout.findViewById(R.id.title_movie)

    fun onBindOnClick(onClickListener: AdapterOnItemClickListener<Movie>, movie: Movie) {
        rootLayout.setOnClickListener {
            onClickListener.onClick(movie)
        }
    }

}
