package mobile.skynet.app.presenter.http.request.retrofit.action;

import mobile.skynet.app.presenter.http.request.retrofit.models.MessageCallback;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by r028367 on 07/03/2018.
 */

public interface CallbackRequestAPI<T, Message extends MessageCallback> {
    void onSuccessRequest(@NotNull T data);
    void onSuccessRequest(@NotNull List<T> data);
    void onFailureRequest(@NotNull Message message);
}
