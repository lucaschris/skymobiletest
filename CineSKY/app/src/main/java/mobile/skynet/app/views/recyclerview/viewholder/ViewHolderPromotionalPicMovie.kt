package mobile.skynet.app.views.recyclerview.viewholder

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import mobile.skynet.app.R
import mobile.skynet.app.views.recyclerview.actions.AdapterOnItemClickListener

class ViewHolderPromotionalPicMovie(private val rootLayout: View) : RecyclerView.ViewHolder(rootLayout) {
    var imageViewPicMovie : ImageView = rootLayout.findViewById(R.id.promotional_image_movie)

    fun onBindClick(adapterOnItemClickListener: AdapterOnItemClickListener<String>, url: String) {
        rootLayout.setOnClickListener {
            adapterOnItemClickListener.onClick(url)
        }
    }
}