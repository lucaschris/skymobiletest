package mobile.skynet.app.views.recyclerview.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import mobile.skynet.app.R
import mobile.skynet.app.utils.LoadImage
import mobile.skynet.app.views.recyclerview.actions.AdapterOnItemClickListener
import mobile.skynet.app.views.recyclerview.viewholder.ViewHolderPromotionalPicMovie

class AdapterPromotionalPictureMovie(val adapterOnItemClickListener: AdapterOnItemClickListener<String>
                                     , val images: List<String>) : RecyclerView.Adapter<ViewHolderPromotionalPicMovie>() {


    private lateinit var picasso: Picasso

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderPromotionalPicMovie {
        picasso = Picasso.get()
        return ViewHolderPromotionalPicMovie(LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_layout_promotional_images_movie, parent, false))
    }

    override fun getItemCount(): Int = images.size


    override fun onBindViewHolder(holder: ViewHolderPromotionalPicMovie, position: Int) {
        val url = images[position]
        LoadImage.load(picasso
            , url
            , R.drawable.placeholder
            , R.drawable.image_not_found
            , holder.imageViewPicMovie
            , "CACHE:$url"
            , "CACHE: $url")

        holder.onBindClick(adapterOnItemClickListener, url)
    }

}