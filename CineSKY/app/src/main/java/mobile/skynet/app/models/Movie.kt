package mobile.skynet.app.models

import android.os.Parcel
import android.os.Parcelable

class Movie() : Parcelable {

    var id: String = ""
    var title: String = ""
    var overview: String = ""
    var releaseYear: String = ""
    var urlCoverImage: String = ""
    var promotionalImages: List<String> = arrayListOf()
    var durationDescription: String = ""

    constructor(
       id: String
       ,title: String
        , overview: String
        , releaseYear: String
        , urlCoverImage: String
        , promotionalImages: List<String> = arrayListOf()
        , durationDescription: String) : this() {
        this.id = id
        this.title = title
        this.overview = overview
        this.releaseYear = releaseYear
        this.urlCoverImage = urlCoverImage
        this.promotionalImages = promotionalImages
        this.durationDescription = durationDescription
    }

    constructor(reader: Parcel) : this() {
        id = reader.readString() ?: "nulp"
        title = reader.readString() ?: "nulp"
        overview = reader.readString() ?: "nulp"
        releaseYear = reader.readString() ?: "nulp"
        urlCoverImage = reader.readString() ?: "nulp"
        durationDescription = reader.readString() ?: "nulp"
        reader.readList(promotionalImages, String::class.java.classLoader)
    }

    override fun writeToParcel(writer: Parcel, flags: Int) {
        writer.writeString(id)
        writer.writeString(title)
        writer.writeString(overview)
        writer.writeString(releaseYear)
        writer.writeString(urlCoverImage)
        writer.writeString(durationDescription)
        writer.writeList(promotionalImages)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Movie> {
        override fun createFromParcel(parcel: Parcel): Movie {
            return Movie(parcel)
        }

        override fun newArray(size: Int): Array<Movie?> {
            return arrayOfNulls<Movie?>(size)
        }
    }

    override fun toString(): String {
        return "Tituto: $title\nResumo: $overview\nDuracao: $durationDescription\nID: $id"
    }

}