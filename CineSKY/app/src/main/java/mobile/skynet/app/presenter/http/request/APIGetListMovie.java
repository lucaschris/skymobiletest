package mobile.skynet.app.presenter.http.request;

import mobile.skynet.app.models.Movie;
import mobile.skynet.app.presenter.http.request.retrofit.action.CallbackRequestAPI;
import mobile.skynet.app.presenter.http.request.retrofit.converter.ParserMovieData;
import mobile.skynet.app.presenter.http.request.retrofit.converter.factory.GenericConverterFactory;
import mobile.skynet.app.presenter.http.request.retrofit.endpoint.EndpointAPIMovies;
import mobile.skynet.app.presenter.http.request.retrofit.models.SimpleMessageCallback;
import mobile.skynet.app.presenter.http.request.retrofit.utils.MediaTypeConstant;
import mobile.skynet.app.presenter.http.request.retrofit.utils.RetrofitServiceGenerator;
import org.jetbrains.annotations.NotNull;
import retrofit2.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class APIGetListMovie {


    /**
     * Metodo responsavel por recuperar a lista de filmes da API.
     *
     * Para atualizar nossa interface grafica usamos um padrao de projetos denominado de delegate.
     * Como a requisicao sera assincrona, para melhor experiencia do usuario, precisamos de uma implementacao
     * responsavel por atualizar a lista de filmes assim que a requisicao terminar, ou mostrar uma mensagem
     * de erro para o usuario no caso de ocorrer um Time out na requisicao
     * */
    public void requestListMovie(String url, final CallbackRequestAPI<Movie, SimpleMessageCallback> callbackRequestAPI) {

        Converter.Factory factory = new GenericConverterFactory<>(new ParserMovieData()
                , MediaTypeConstant.MEDIA_TYPE_JSON);
        EndpointAPIMovies endpointAPIMovies = RetrofitServiceGenerator.getService(EndpointAPIMovies.class, factory, url);

        Call<List<Movie>> call = endpointAPIMovies.getMovies();

        call.enqueue(new Callback<List<Movie>>() {
            @Override
            public void onResponse(@NotNull Call<List<Movie>> call, @NotNull Response<List<Movie>> response) {
                if (response.isSuccessful()) {
                    List<Movie> result = response.body();
                    callbackRequestAPI.onSuccessRequest(result != null ? result : new ArrayList<Movie>());
                }

                else {
                    SimpleMessageCallback s = new SimpleMessageCallback(String.format(Locale.getDefault()
                            ,"Erro %d", response.code()));
                    callbackRequestAPI.onFailureRequest(s);
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<Movie>> call, @NotNull Throwable t) {
                SimpleMessageCallback s;
                if (t instanceof IOException) {
                    // possivelmente uma excecao que
                    s = new SimpleMessageCallback("Problemas com a conexão com a internet");
                }
                else {
                    s = new SimpleMessageCallback("Ocorreu um erro ao tentarmos consultar a lista de filmes.");
                }

                callbackRequestAPI.onFailureRequest(s);
            }
        });

    }
}