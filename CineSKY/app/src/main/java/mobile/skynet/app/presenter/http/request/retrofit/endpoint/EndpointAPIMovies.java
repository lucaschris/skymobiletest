package mobile.skynet.app.presenter.http.request.retrofit.endpoint;

import mobile.skynet.app.models.Movie;
import retrofit2.Call;
import retrofit2.http.GET;

import java.util.List;

public interface EndpointAPIMovies {

    @GET("api/Movies")
    Call<List<Movie>> getMovies();
}
