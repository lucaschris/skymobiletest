package mobile.skynet.app.presenter.http.request.retrofit.converter;

import mobile.skynet.app.models.Movie;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ParserMovieData implements ParserData<Movie, List<Movie>> {


    @Override
    public String fromObjectToSomeFormattedData(Movie data) throws Exception {
        return "";
    }

    @Override
    public List<Movie> fromFormattedDataToObject(String data) throws Exception {
        List<Movie> movies = new ArrayList<>();
        JSONArray jsonArrayMovies = new JSONArray(data);
        for (int i = 0; i < jsonArrayMovies.length() ; i++) {
            JSONObject jsonMovie = jsonArrayMovies.getJSONObject(i);
            List<String> urlImagePromotional = new ArrayList<>();
            JSONArray jsonUrlImagePromotional = jsonMovie.getJSONArray("backdrops_url");
            for (int j = 0; j <jsonUrlImagePromotional.length(); j++) {
                urlImagePromotional.add(jsonUrlImagePromotional.get(j).toString());
            }
            Movie movie = new Movie(
                jsonMovie.getString("id")
                , jsonMovie.getString("title")
                , jsonMovie.getString("overview")
                , jsonMovie.getString("release_year")
                , jsonMovie.getString("cover_url")
                , urlImagePromotional
                , jsonMovie.getString("duration")
            );
            movies.add(movie);
        }
        return movies;
    }
}
