package mobile.skynet.app.views.recyclerview.actions

interface AdapterOnItemClickListener<T> {
    fun onClick(item: T)
}