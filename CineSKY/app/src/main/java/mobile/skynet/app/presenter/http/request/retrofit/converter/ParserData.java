package mobile.skynet.app.presenter.http.request.retrofit.converter;


public interface ParserData<Request,Response> {
    String fromObjectToSomeFormattedData(Request data) throws Exception;
    Response fromFormattedDataToObject(String data) throws Exception;
}
