package mobile.skynet.app.presenter.http.request.retrofit.converter;

import org.jetbrains.annotations.NotNull;

public interface IConverterRequestGeneric<T> {
   @NotNull
   String convertRequestOnJson(T modelo);
}
