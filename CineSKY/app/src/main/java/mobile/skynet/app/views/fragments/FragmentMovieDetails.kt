package mobile.skynet.app.views.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import mobile.skynet.app.R
import mobile.skynet.app.models.Movie
import mobile.skynet.app.views.recyclerview.actions.AdapterOnItemClickListener
import mobile.skynet.app.views.recyclerview.adapter.AdapterPromotionalPictureMovie


/**
 * A simple [Fragment] subclass.
 * Use the [FragmentMovieDetails.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class FragmentMovieDetails : BaseFragment(), AdapterOnItemClickListener<String> {

    private lateinit var adapterListPicMovies : AdapterPromotionalPictureMovie


    val BUNDLE_MOVIE = "BUNDLE_MOVIE"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tagFragment = javaClass.simpleName
    }

    // Ao clicar numa imagem promocional podemos abrir um DialogFragment para mostrar a imagem numa maior
    // dimensao e ate mesmo usar uma custom ImageView redimensionavel para imagens com baixa resolucao
    override fun onClick(item: String) {
        Toast.makeText(context, item, Toast.LENGTH_LONG).show()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val viewRoot = inflater.inflate(R.layout.fragment_movie_details, container, false)

        val recyclerViewPicMovies = viewRoot.findViewById<RecyclerView>(R.id.list_promotional_image_movie)

        adapterListPicMovies = AdapterPromotionalPictureMovie(this, movie.promotionalImages)

        recyclerViewPicMovies.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recyclerViewPicMovies.adapter = adapterListPicMovies


        activity?.title = movie.title

        viewRoot.findViewById<TextView>(R.id.release_year).text = "Ano: ${movie.releaseYear}"
        viewRoot.findViewById<TextView>(R.id.text_overview).text = movie.overview
        viewRoot.findViewById<TextView>(R.id.time_duration_movie).text = "Duração: ${movie.durationDescription}"

        return viewRoot
    }

    private lateinit var movie: Movie

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentMovieDetails.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(movie: Movie) =
            FragmentMovieDetails().apply {
                this.movie = movie
            }
    }
}
