package mobile.skynet.app.presenter.http.request.retrofit.converter.factory;

import android.util.Log;
import mobile.skynet.app.presenter.http.request.retrofit.converter.IConverterRequestGeneric;
import mobile.skynet.app.presenter.http.request.retrofit.converter.IConverterResponseGeneric;
import mobile.skynet.app.presenter.http.request.retrofit.converter.ParserData;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import org.jetbrains.annotations.NotNull;
import retrofit2.Converter;
import retrofit2.Retrofit;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

public class GenericConverterFactory<ObjRequest, ObjResponse> extends Converter.Factory {

    private ParserData<ObjRequest, ObjResponse> parserData;
    private String mediaType;

    public GenericConverterFactory(ParserData<ObjRequest, ObjResponse> parserData, String mediaType) {
        this.parserData = parserData;
        this.mediaType = mediaType;
    }

    @Override
    public Converter<ResponseBody, ObjResponse> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        return new Converter<ResponseBody, ObjResponse>() {
            @Override
            public ObjResponse convert(@NotNull ResponseBody value) throws IOException {
                return (new IConverterResponseGeneric<ObjResponse>() {
                    @Override
                    public ObjResponse convertResponseOnObject(String responseBody) {
                        try {
                            return parserData.fromFormattedDataToObject(responseBody);
                        } catch (Exception e) {
                            Log.e("EX_GEN_CONVERTER_RQ", e.getMessage());
                        }
                        return null;
                    }
                }).convertResponseOnObject(value.string());
            }
        };
    }


    @Override
    public Converter<ObjRequest, RequestBody> requestBodyConverter(Type type, Annotation[] parameterAnnotations, Annotation[] methodAnnotations, Retrofit retrofit) {
        return new Converter<ObjRequest, RequestBody>() {

            @Override
            public RequestBody convert(@NotNull ObjRequest objectBody) throws IOException {
                IConverterRequestGeneric<ObjRequest> converter = new IConverterRequestGeneric<ObjRequest>() {
                    @NotNull
                    @Override
                    public String convertRequestOnJson(ObjRequest objectBody) {
                        try {
                            return parserData.fromObjectToSomeFormattedData(objectBody);
                        } catch (Exception e) {
                            Log.e("EX_GEN_CONVERTER_RQ", e.getMessage());
                        }
                        return "";
                    }
                };
                return RequestBody.create(MediaType.parse(mediaType), converter.convertRequestOnJson(objectBody));
            }
        };
    }
}
