package mobile.skynet.app.utils

import android.util.Log
import android.widget.ImageView

import com.squareup.picasso.Callback
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import java.lang.Exception


class LoadImage {

    companion object {
        fun load( picasso: Picasso
                  ,  url: String
                  , placeholder: Int
                  , error: Int
                  , imageView: ImageView
                  , msgOnSuccessLoad: String
                  , msgOnErrorLoad: String
        ) {
            picasso.run {
                //setIndicatorsEnabled(true)
                load(url)
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .placeholder(placeholder)
                    .error(error)
                    //.resize(512, 512)
                    .into(imageView, object : Callback {
                        override fun onSuccess() {
                            Log.i("DOWNLOAD_IMG_COVER", msgOnSuccessLoad)
                        }

                        override fun onError(e: Exception?) {
                            Log.i("DOWNLOAD_IMG_COVER_ERR", msgOnErrorLoad)
                            picasso.run {
                                load(url)
                                    .placeholder(placeholder)
                                    //.resize(512, 512)
                                    .error(error)
                                    .into(imageView, object : Callback {
                                        override fun onSuccess() {
                                            Log.i("DOWNLOAD_IMG_COVER", msgOnSuccessLoad)

                                        }

                                        override fun onError(e: Exception?) {
                                            Log.i("DOWNLOAD_IMG_COVER_ERR", msgOnErrorLoad)
                                        }
                                    })
                            }
                        }
                    })
            }
        }
    }
}

